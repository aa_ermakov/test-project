<?php

require ("login.php");

$name = null;
$email =null;

$namevalidationmessage = null;
$emailvalidationmessage = null;


if ($_SERVER ['REQUEST_METHOD'] == 'POST')
{

	$name = $_POST['name'];
	$email = $_POST['email']; 

	$isValid = true;
	$pattern = '/^[a-zA-Z0-9][a-zA-Z0-9\._\-&=#]*@/';

	if (empty($name))
	{
		$namevalidationmessage = "Поле name является обязательным для заполнения";
		$isValid = false;
	}

	if (empty($email))
	{
		$emailvalidationmessage = "Поле email является обязательным для заполнения";
		$isValid = false;
	}
	
	$isInvalidEmail = !preg_match($pattern, $email) 
	|| (!checkdnsrr(preg_replace($pattern, '', $email))); //'/[^@]+@/', вариант Юры
	
	if ($isInvalidEmail)  //не соответствует шаблону до @	
		//не соответствует домен $email
	{
		$emailvalidationmessage = "Укажите корректный email адрес";
		$isValid = false;
	}
		
	
	
	if ($isValid == true)
	{
		
		$query = $dsn->prepare('INSERT INTO users(name, email) VALUES (?, ?)');
		$query->execute (array($name, $email));
	
		header("Location: /test2/indexpdo.php");
		exit;
	
	}
}

?>
		
<form method="post">
	<lable> name </lable><input type="text" name="name" value= "<?php echo $name; ?>"
	 size=35 placeholder="Введите имя пользователя"/> 
<?php if ($namevalidationmessage !== null) { echo "<br><font color=\"#ff0000\">$namevalidationmessage</font>";} ?>
<br>
	<lable> email </lable><input type="text" name="email" value= "<?php echo $email; ?>"
	size=35 placeholder="Введите адрес электронной почты"/> 
<?php if ($emailvalidationmessage !== null) {echo "<br><font color=\"#ff0000\">$emailvalidationmessage</font>";} ?>
<br>
	<input type="submit" name="submit" value="Создать"/>
</form>

<td><a href="indexpdo.php"> Назад к списку пользователей </a></td>


