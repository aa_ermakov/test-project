<?php

require "login.php";

if (!isset ($_GET['id']))
{
	echo 'ID не существует'; // если false, то пойдет продолжение вне блока
	exit;
}

$id = $_GET['id'];

$query = $dsn-> prepare ('SELECT * FROM users WHERE id = :id'); //почему не WHERE id=?
$query -> execute ([':id' => $id]); //почему используется подготовленный запрос?
$row = $query -> fetch (PDO::FETCH_ASSOC); //пробовал без этого, ругался что он нужен


 if (empty ($row))
 {
	 echo "Строка из базы не существует"; 
	 exit;
 }

$name = $row['name'];
$email = $row['email'];

$namevalidationmessage = null;
$emailvalidationmessage = null;



if ($_SERVER ['REQUEST_METHOD'] == 'POST')
{
	$name = $_POST['name'];
	$email = $_POST['email'];
	$isValid = true;
	$pattern = '/^[a-zA-Z0-9][a-zA-Z0-9\._\-&=#]*@/';


	if (empty($name)) 
	{
		$namevalidationmessage = "Поле name является обязательным для заполнения";
		$isValid = false;
	}

	if (empty($email))
	{
		$emailvalidationmessage = "Поле email является обязательным для заполнения";
		$isValid = false;	
	}

	$isInvalidEmail = !preg_match($pattern, $email) 
	|| (!checkdnsrr(preg_replace($pattern, '', $email)));

	if ($isInvalidEmail)
	{
		$emailvalidationmessage = "Укажите корректный email адрес";
		$isValid = false;
	}	

	if ($isValid == true) 
	{
		$query = $dsn->prepare("UPDATE users SET name= ?, email= ? WHERE id= ?"); 
    	$query->execute (array($name, $email, $id));

    	
    	header("Location: /test2/indexpdo.php");
		exit;	
	}
}

?>

<form method="post">
      <lable>name </lable><input type="text" name="name" 
     value= "<?php echo $name; ?>"/>
<?php if ($namevalidationmessage !== null) { echo "<br><font color=\"#ff0000\">$namevalidationmessage</font>";} ?>
<br>
     <lable> email </lable><input type="text" name="email" 
     value= "<?php echo $email; ?>" />
 <?php if ($emailvalidationmessage !== null) { echo "<br><font color=\"#ff0000\">$emailvalidationmessage</font>"; } ?>
 <br>
     <input type="submit" name="submit" value="Изменить" />
</form>
